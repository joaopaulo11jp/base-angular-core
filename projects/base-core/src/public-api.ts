/*
 * Public API Surface of base-core
 */
//export * from './core/constants';
//export * from './core/enums';
//export * from './core/types';
export * from './core';
export * from './core/service/remote';
export * from './core/service/base.service';
export * from './core/decorator';
export * from './core/interfaces';
export * from './core/service';
export * from './core/events';

export {CoreModule} from './core/core.module';