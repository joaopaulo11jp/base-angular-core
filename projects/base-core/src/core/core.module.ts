import { NgModule, Injector } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseServiceModule } from './service';
import { HttpClientModule } from '@angular/common/http';

//@dynamic
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BaseServiceModule,
    HttpClientModule
  ],
  providers: [],
  exports: [BaseServiceModule, HttpClientModule]
})
export class CoreModule {
  // static staticInjector = Injector.create({
  //   /** static injectors for decorators */
  //   providers: [
  //     { provide: HttpClient, deps: [HttpHandler] },
  //     { provide: HttpHandler, useValue: new HttpXhrBackend({ build: () => new XMLHttpRequest }) },
  //   ],
  // });
}
