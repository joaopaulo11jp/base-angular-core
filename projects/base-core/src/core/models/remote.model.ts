import { Expose } from 'class-transformer';

export type IdType = string | number;

interface RemoteModelInterface {
  id: IdType;
}

export class RemoteModel implements RemoteModelInterface {
  @Expose()
  id: IdType;
}
