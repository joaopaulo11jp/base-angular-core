import { Page } from '../interfaces';
import { Expose } from 'class-transformer';
import { RemoteModel } from './remote.model';

export class PageModel<T extends RemoteModel> implements Page<T> {
  @Expose()
  content: Array<T>;
  @Expose()
  size: number;
  @Expose()
  totalPage: number;
  @Expose()
  totalElements: number;
}
