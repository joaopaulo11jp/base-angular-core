import { InjectionToken } from '@angular/core';
import * as constants from '../constants';
import { RemoteModel } from '../models';

export type RemoteModelType = RemoteModel;

export const HTTP_CONFIG = new InjectionToken<string>(constants.BASE_REMOTE_CONFIG);
