export const REMOTE_CLASS_METADATA = 'remote_class_metadata';
export const REMOTE_MAPPING_METADATA = 'remote_mapping_metadata';
export const ID_REMOTE_PARAMETER_METADATA = 'id_remote_parameter_metadata';
export const BODY_REMOTE_PARAMETER_METADATA = 'body_remote_parameter_metadata';
export const SERIALIZER_REMOTE_PARAMETER_METADATA = 'serializer_remote_parameter_metadata';
export const BASE_REMOTE_CONFIG = 'remote_config';
export const APPLICATION_BOOTSTRAP_EVENT = 'application_bootstrap_event';
