import { Observable } from 'rxjs';
import { Type } from '@angular/core';
import { HttpHeaders } from '@angular/common/http/src/headers';
import { RemoteSerializer } from '../service/remote/serializer';
import { HttpParams } from '@angular/common/http/src/params';
import { RemoteModel } from '../models';

export interface OptionsHttp {
  headers?: HttpHeaders | {
    [header: string]: string | string[];
  };
  observe?: 'body';
  params?: HttpParams | {
    [param: string]: string | string[];
  };
  reportProgress?: boolean;
  responseType: 'arraybuffer';
  withCredentials?: boolean;
}

export interface RemoteParams {
  apiToken?: string;
  optionsHttp?: OptionsHttp | any;
  serializer?: RemoteSerializer;
}

export interface BaseRemoteInterface<T extends RemoteModel> {
  get<M extends RemoteModel>(url: string, model?: Type<M>, params?: RemoteParams): Observable<M|T>;
  post<M extends RemoteModel>(url: string, body: M, model: Type<M>, params?: RemoteParams): Observable<M|T>;
  put<M extends RemoteModel>(url: string, body: M, model: Type<M>, params?: RemoteParams): Observable<M|T>;
  delete(url: string, params?: RemoteParams): Observable<any>;
  plainModel<M>(data: any, model: Type<M|T>): M|T;
}
