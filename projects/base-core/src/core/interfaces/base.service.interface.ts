import { Type } from '@angular/core';
import { Observable } from 'rxjs';
import { RemoteParams } from './base.remote.interface';
import { IdType, PageModel, RemoteModel } from '../models';
import { BaseRemote } from '../service/remote';


export interface BaseRemoteOptions<T extends RemoteModel> {
  url?: string;
  model?: Type<T>;
  params?: RemoteParams;
}

export interface BaseServiceDecorator<M extends RemoteModel, T extends BaseRemote<M>> {
  model: Type<M>;
  remote: Type<T>;
}

export interface BaseServiceInterface<M extends RemoteModel, T extends BaseRemote<M>> {
  remote: T;

  findOne(id: IdType, options?: BaseRemoteOptions<M>): Observable<M>;
  list(options?: BaseRemoteOptions<M>): Observable<M[]>;
  page(options?: BaseRemoteOptions<M>): Observable<PageModel<M>>;
  save(entity: M, options?: BaseRemoteOptions<M>): Observable<M>;
  update(entity: M, options?: BaseRemoteOptions<M>): Observable<M>;
  remove(id: IdType, options?: BaseRemoteOptions<M>): Observable<any>;
}
