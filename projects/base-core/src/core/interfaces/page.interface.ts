import { RemoteModel } from '../models';

export interface Page<T extends RemoteModel> {
  content: Array<T>;
  size: number;
  totalPage: number;
  totalElements: number;
}
