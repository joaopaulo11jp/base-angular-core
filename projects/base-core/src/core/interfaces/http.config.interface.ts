export interface HttpConfig {
  apiUrl: string;
  timeoutRequest: number;
  others?: HttpConfigOther[];
}

export interface HttpConfigOther extends HttpConfig {
  tokenApi: string;
}

export interface HttpConfigInterface {
  createHttpConfig(): HttpConfig;
}
