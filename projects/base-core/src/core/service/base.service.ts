import { Injectable, Injector, Type } from '@angular/core';
import { BaseRemoteOptions, BaseServiceInterface } from '../interfaces';
import { Observable } from 'rxjs';
import { map } from 'rxjs/internal/operators';
import { IdType, PageModel, RemoteModel } from '../models';
import { BaseRemote } from './remote';

@Injectable()
export class BaseService<M extends RemoteModel, T extends BaseRemote<M>> implements BaseServiceInterface<M, T> {

  protected modelType: Type<M>;
  protected remoteType: Type<T>;

  constructor(private readonly injector: Injector) { }

  public findOne(id: IdType, options?: BaseRemoteOptions<M>): Observable<M> {
    const url = options && options.url ? `${this.remote.baseUrl + options.url}/${id}` : `${this.remote.baseUrl}/${id}`;
    const model = options && options.model;
    const params = options && options.params;
    return this.remote.get(url, model || this.modelType, params || {});
  }

  public list(options?: BaseRemoteOptions<M>): Observable<M[]> {
    const url = options && options.url ? `${this.remote.baseUrl + options.url}` : `${this.remote.baseUrl + options.url}/list`;
    const model = options && options.model;
    const params = options && options.params;

    return this.remote.get<M>(url, model || this.modelType, params || {})
      .pipe(
        map((res: any | M[]) => {
          return res.map(value => this.remote.plainModel(value, model || this.modelType));
        })
      );
  }

  public page(options?: BaseRemoteOptions<M>): Observable<PageModel<M>> {
    const url = options && options.url ? `${this.remote.baseUrl + options.url}` : this.remote.baseUrl;
    const model = options && options.model;
    const params = options && options.params;

    return this.remote.get(url, null, params || {})
      .pipe(
        map((res: any | PageModel<M>) => {
          const page = this.remote.plainModel<PageModel<M>>(res, PageModel);
          page.content = res.content.map(value => this.remote.plainModel(value, model || this.modelType));

          return page;
        })
      );
  }

  public save(entity: M, options?: BaseRemoteOptions<M>): Observable<M> {
    const url = options && options.url ? `${this.remote.baseUrl + options.url}` : this.remote.baseUrl;
    const model = options && options.model;
    return this.remote.post(url, entity, model || this.modelType);
  }

  public update(entity: M, options?: BaseRemoteOptions<M>): Observable<M> {
    const url = options && options.url ? `${this.remote.baseUrl + options.url}` : this.remote.baseUrl;
    const params = options && options.params;
    return this.remote.put(url, entity, options.model, params || {} );
  }

  public remove(id: IdType, options?: BaseRemoteOptions<M>): Observable<any> {
    const url = options && options.url ? `${this.remote.baseUrl + options.url}/${id}` : `${this.remote.baseUrl}/${id}`;
    return this.remote.delete(url);
  }

  get remote(): T {
    return this.injector.get<T>(this.remoteType);
  }

}
