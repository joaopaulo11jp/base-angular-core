import { NgModule } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClientModule } from '@angular/common/http';
import { BaseRemoteModule } from './remote';

@NgModule({
  imports: [
    HttpClientModule,
    BaseRemoteModule
  ],
  providers: [
    BaseService,
  ],
  exports: [HttpClientModule, BaseRemoteModule]
})
export class BaseServiceModule { }
