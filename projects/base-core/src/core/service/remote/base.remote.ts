import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Inject, Injectable, Type } from '@angular/core';
import { plainToClass } from 'class-transformer';
import { HttpConfig, HttpConfigInterface, BaseRemoteInterface, RemoteParams } from '../../interfaces';
import { RemoteModel } from '../../models';
import { HTTP_CONFIG } from '../../types';
import { timeout } from 'rxjs/internal/operators';

@Injectable()
export class BaseRemote<T extends RemoteModel> implements BaseRemoteInterface<T> {

  public baseUrl: string;

  constructor(
    @Inject(HTTP_CONFIG) private readonly httpConfig: HttpConfigInterface,
    @Inject(HttpClient) public readonly http: HttpClient
  ) { }

  public get<M extends RemoteModel>(
    url: string,
    model?: Type<M|T>,
    params: RemoteParams = {}): Observable<M|T> {
    const { apiUrl, timeoutRequest } = this.getHttpConfig(params);

    return this.http.get<M | T>(apiUrl + url, params.optionsHttp)
      .pipe(
        timeout(timeoutRequest) // TODO implementar tratamento de exceção para timeout de conexão
      ).pipe(
        map(data => this.plainModel<M | T>(data, model))
      );
  }

  public post<M extends RemoteModel>(url: string, body: M|T, model: Type<M|T>, params: RemoteParams = {}): Observable<M|T> {
    const { apiUrl, timeoutRequest } = this.getHttpConfig(params);

    return this.http.post<M | T>(apiUrl + url, body, params.optionsHttp)
      .pipe(
        timeout(timeoutRequest)
      )
      .pipe(
        map(data => this.plainModel<M | T>(data, model))
      );
  }

  public put<M extends RemoteModel>(url: string, body: M|T, model: Type<M|T>, params: RemoteParams = {}): Observable<M|T> {
    const { apiUrl, timeoutRequest } = this.getHttpConfig(params);

    return this.http.put<M | T>(apiUrl + url, body, params.optionsHttp)
      .pipe(
        timeout(timeoutRequest)
      )
      .pipe(
        map(data => this.plainModel<M | T>(data, model))
      );
  }

  public delete<M extends RemoteModel>(url: string, params: RemoteParams = {}): Observable<any> {
    const { apiUrl, timeoutRequest } = this.getHttpConfig(params);

    return this.http.delete<M>(apiUrl + url, params.optionsHttp).pipe(
      timeout(timeoutRequest)
    );
  }

  public plainModel<M>(data: any, model: Type<M>): M {
    return model ? plainToClass(model, data, {strategy: 'excludeAll'}) : data;
  }

  private getHttpConfig(remoteParams?: RemoteParams): HttpConfig {
    if (remoteParams && remoteParams.apiToken) {
      return this.httpConfig.createHttpConfig().others.find(config => config.tokenApi === remoteParams.apiToken);
    }
    return this.httpConfig.createHttpConfig();
  }
}
