import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BaseRemote } from './base.remote';

@NgModule({
  imports: [
    HttpClientModule
  ],
  providers: [
    BaseRemote,
  ],
  exports: [HttpClientModule]
})
export class BaseRemoteModule {
}
