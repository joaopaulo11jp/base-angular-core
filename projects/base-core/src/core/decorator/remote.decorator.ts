import 'reflect-metadata';

import * as constants from '../constants';
import { Type } from '@angular/core';
import { BODY_REMOTE_PARAMETER_METADATA, ID_REMOTE_PARAMETER_METADATA } from '../constants';
import { RemoteModel } from '../models';
import { BaseRemote } from '../service/remote';

export interface RemoteInterface {
  baseUrl?: string;
  apiToken?: string;
}

const defaultMetadata = {
  baseUrl: ''
};

export const Remote = <M extends RemoteModel>(
  params: RemoteInterface = defaultMetadata
) => {
  return <R extends Type<BaseRemote<M>>>(target: R) => {
    target.prototype.baseUrl = params.baseUrl;
    target.prototype.apiToken = params.apiToken;
    Reflect.defineMetadata(constants.REMOTE_CLASS_METADATA, params, target);
  };
};

export const Id = (): ParameterDecorator => (target: Object, propertyName: string, index: number) => {
  Reflect.defineMetadata(ID_REMOTE_PARAMETER_METADATA, index, target, propertyName);
};

export const Body = <M extends RemoteModel>(model?: Type<M>): ParameterDecorator =>
  (target: Object, propertyName: string, index: number) => {
    Reflect.defineMetadata(BODY_REMOTE_PARAMETER_METADATA, index, target, propertyName);
  };
