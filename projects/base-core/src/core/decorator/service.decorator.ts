import 'reflect-metadata';

import { BaseServiceDecorator } from '../interfaces';
import { BaseRemote } from '../service/remote';
import { RemoteModel } from '../models';
import { Type } from '@angular/core/core';
import { BaseService } from '../service';

export const Service = <M extends RemoteModel, T extends BaseRemote<M>>(params: BaseServiceDecorator<M, T>) => {
  return <S extends Type<BaseService<M, T>>>(target: S) => {
    target.prototype.modelType = params.model;
    target.prototype.remoteType = params.remote;
  };
};
