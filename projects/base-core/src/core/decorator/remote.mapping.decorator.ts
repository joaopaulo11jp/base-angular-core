import 'reflect-metadata';

import { Type } from '@angular/core';
import { RemoteMethod } from '../enums';
import * as constants from '../constants';
import { BaseRemoteInterface, RemoteParams } from '../interfaces';
import { RemoteModelType } from '../types';
import { RemoteInterface } from './remote.decorator';
import { IdType, RemoteModel } from '../models';

interface RequestMappingMetadata {
  model?: Type<RemoteModelType>;
  method?: keyof BaseRemoteInterface<RemoteModelType>;
  params?: RemoteParams;
}

const defaultMetadata: RequestMappingMetadata = {
  model: undefined,
  method: undefined,
  params: { optionsHttp: {} }
};

const RemoteMapping = <M extends RemoteModel>(
  url: string,
  metadata: RequestMappingMetadata = defaultMetadata,
  remoteMethod: RemoteMethod
): MethodDecorator => {
  url = url || '/';
  const { method, model, params } = metadata;

  return (target: Object, key, descriptor: PropertyDescriptor & any) => {

    descriptor.value = function(...args: any[]) {
      const remote: RemoteInterface = Reflect.getMetadata(constants.REMOTE_CLASS_METADATA, target.constructor);
      const indexId: number = Reflect.getMetadata(constants.ID_REMOTE_PARAMETER_METADATA, target, key);

      const id: IdType = args[indexId];
      const body = { };
      const { baseUrl } = remote || { baseUrl: '/' };

      if (method) {
        return this[method](baseUrl + url);
      }

      if (this.apiToken) {
        params.apiToken = this.apiToken;
      }

      switch (remoteMethod) {
        case RemoteMethod.GET:
          return this.get(baseUrl + url, model || this.model, params);
        case RemoteMethod.POST:
          return this.post(baseUrl + url, { id }, body, model || this.model, params);
        case RemoteMethod.PUT:
          return this.put(baseUrl + url, { id }, body, model || this.model, params);
        case RemoteMethod.DELETE:
          return this.delete(baseUrl + url, params);
        default:
          return this.get(baseUrl + url, model || this.model, params);
      }
    };

    Reflect.defineMetadata(constants.REMOTE_MAPPING_METADATA, { url, options: metadata, remoteMethod }, descriptor.value );
    return descriptor;
  };
};

const createMappingHttpDecorator = (methodMapping: RemoteMethod) => (
  url?: string,
  params?: RequestMappingMetadata,
): MethodDecorator => {
  return RemoteMapping (url, params, methodMapping);
};


export const Get = createMappingHttpDecorator(RemoteMethod.GET);
export const Post = createMappingHttpDecorator(RemoteMethod.POST);
export const Put = createMappingHttpDecorator(RemoteMethod.PUT);
export const Delete = createMappingHttpDecorator(RemoteMethod.DELETE);
